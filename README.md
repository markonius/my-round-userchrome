# My UserChrome

## Installation

Clone this repository directly into `your-ff-profile/chrome` or just download [userChrome.css](userChrome.css) and place it there. More info [here](https://www.reddit.com/r/FirefoxCSS/comments/73dvty/tutorial_how_to_create_and_livedebug_userchromecss/).

Please report how it works on Windows or Mac.

## DuckDuckGo frontend

There's also this:
[DuckDuckGo](https://markonius.gitlab.io/lorem-ducksum/)

For best experience, install [CORS Everywhere](https://addons.mozilla.org/en-US/firefox/addon/cors-everywhere/) and enable it for this site.  
Add "/https:\/\/markonius.gitlab.io.*/i" to its whitelist.
